
#if 0
CXX=g++;
includes="-I /home/vagrant/tools/snappy/include";
libs="-L /home/vagrant/tools/snappy/lib  -l snappy";
program='DumpIndex';
${CXX}  ${includes}  -g -O0  -o ${program}.exe  ${program}.cpp  ${libs};
exit  0
#endif

#include    <iomanip>
#include    <iostream>
#include    <iterator>
#include    <stdio.h>
#include    <string>
#include    <vector>

#include    <snappy.h>


typedef     const  uint8_t  *       Buffer;

template  <typename T>
T
pointer_cast(void * ptr)
{
    return ( static_cast<T>(ptr) );
}

template  <typename T>
T
pointer_cast(const void * ptr)
{
    return ( static_cast<T>(ptr) );
}

template  <typename  T>
T
readVarInt(
        Buffer          & ptr,
        const   Buffer  ptrEnd,
        int64_t         & pos)
{
    T   retVal  = 0;
    int nShift  = 0;
    while ( ptr < ptrEnd ) {
        const  uint8_t  b8  = *(ptr ++);
        ++  pos;
        retVal  |= ((b8 & 0x7F) << nShift);
        nShift  += 7;
        if ( !(b8 & 0x80) ) {
            return ( retVal );
        }
    }

    pos = -1;
    return ( retVal );
}

template  <typename  T, size_t  NUM_BYTES>
T
readFix(
        Buffer          & ptr,
        const   Buffer  ptrEnd,
        int64_t         & pos)
{
    T   retVal  = 0;
    retVal  =  *(pointer_cast<const T *>(ptr));
    ptr     += NUM_BYTES;
    pos     += NUM_BYTES;

    return ( retVal );
}

template  <typename  T>
int64_t
parseProtocolBuffer(
        const   Buffer      ptrBegin,
        const   Buffer      ptrEnd,
        const   int64_t     offset,
        T *  const          ptrData,
        const   int64_t     depth = 0)
{
    int64_t     posCur  = offset;
    uint32_t    valU32  = 0;
    uint64_t    valU64  = 0;
    uint64_t    valLen  = 0;

#if defined( _DEBUG )
    char    temp[1024];
    sprintf(temp, "begin = %p, end = %p, offset=%lx",
            ptrBegin, ptrEnd, offset);
    std::cerr   <<  temp    <<  std::endl;
#endif

    for ( Buffer ptr = ptrBegin; ptr < ptrEnd; ) {
        int64_t     posNew  = posCur;

        const  int  tmp = readVarInt<int>(ptr, ptrEnd, posNew);
        const  int  tag = (tmp >> 3);
        const  int  mod = (tmp &  7);

        switch ( mod ) {
        case  0:
            valU64  = readVarInt<uint64_t>(ptr, ptrEnd, posNew);
            ptrData->setInt(tag, valU64);
            break;
        case  1:
            valU64  = readFix<uint64_t, 8>(ptr, ptrEnd, posNew);
            valLen  = 8;
            ptr     += valLen;
            posNew  += valLen;
            break;
        case  2:
            valLen  = readVarInt<uint64_t>(ptr, ptrEnd, posNew);
            ptrData->setSubObject(
                    tag, ptr, ptr + valLen, posNew, depth + 1
            );
            ptr     += valLen;
            posNew  += valLen;
            break;
        case  3:
            break;
        case  4:
            break;
        case  5:
            valU32  = readFix<uint32_t, 4>(ptr, ptrEnd, posNew);
            ptrData->setInt(tag, valU32);
            break;
        default:
            std::cerr   <<  "Invalid Mode :"    <<  mod
                        << " @ 0x"
                        <<  std::hex    <<  std::setw(8)
                        <<  posCur      <<  std::endl;
            return ( -1 );
        }

        posCur  = posNew;
    }

    return ( posCur );
}

struct  Dim
{
    int64_t     m_size;

    Dim()
        : m_size(0)
    { }

    void  setInt(int tag, uint64_t val)
    {
        switch ( tag ) {
        case  1:    this->m_size    = val;  break;
        }
    }

    void  setSubObject(
            const   int         tag,
            const   Buffer      ptrBegin,
            const   Buffer      ptrEnd,
            const   int64_t     offset,
            const   int64_t     depth)
    {
    }

};

struct  TensorShape
{
    std::vector<Dim>    m_dim;

    TensorShape()
        : m_dim()
    { }

    void  setInt(int tag, uint64_t val)
    {
    }

    void  setSubObject(
            const   int         tag,
            const   Buffer      ptrBegin,
            const   Buffer      ptrEnd,
            const   int64_t     offset,
            const   int64_t     depth)
    {
        int64_t     pos = offset;
        switch ( tag ) {
        case  2:
            Dim     obj;
            parseProtocolBuffer(ptrBegin, ptrEnd, pos, &obj, depth);
            this->m_dim.push_back(obj);
            break;
        }
    }
};

struct  BundleEntry
{
    int         m_dtype;
    TensorShape m_shape;
    uint64_t    m_shardId;
    uint64_t    m_offset;
    uint64_t    m_size;
    uint32_t    m_crc32;

    BundleEntry()
        : m_dtype(0),
          m_shape(),
          m_shardId(0),
          m_offset(0),
          m_size(0),
          m_crc32(0)
    { }

    void  setInt(int tag, uint64_t val)
    {
        switch ( tag ) {
        case  1:    this->m_dtype   = val;  break;
        case  3:    this->m_shardId = val;  break;
        case  4:    this->m_offset  = val;  break;
        case  5:    this->m_size    = val;  break;
        case  6:    this->m_crc32   = val;  break;
        }
    }

    void  setSubObject(
            const   int         tag,
            const   Buffer      ptrBegin,
            const   Buffer      ptrEnd,
            const   int64_t     offset,
            const   int64_t     depth)
    {
        int64_t     pos = offset;
        switch ( tag ) {
        case  2:
            parseProtocolBuffer(
                    ptrBegin, ptrEnd, pos,
                    &(this->m_shape), depth);
            break;
        }
    }
};

std::ostream  &
operator << (
        std::ostream    & outStr,
        const   Dim     & dim)
{
    outStr  <<  std::dec    <<  (dim.m_size);
    return ( outStr );
}

std::ostream  &
operator << (
        std::ostream        & outStr,
        const  TensorShape  & shapeInfo)
{
    std::copy(
            shapeInfo.m_dim.begin(),
            shapeInfo.m_dim.end(),
            std::ostream_iterator<Dim>(outStr, ",")
    );
    return ( outStr );
}

std::ostream  &
operator << (
        std::ostream        & outStr,
        const  BundleEntry  & tfInfo)
{
    outStr  <<  "\tdtype   = "
            <<  std::dec    <<  (tfInfo.m_dtype)
            <<  "\n\tshape   = ";
    outStr  <<  tfInfo.m_shape;
    outStr  <<  "\n\tshard   = "
            <<  std::dec    <<  (tfInfo.m_shardId)
            <<  std::setfill('0')
            <<  "\n\toffset  = 0x"
            <<  std::hex    <<  std::setw(8)
            <<  (tfInfo.m_offset)
            <<  " ("    <<  std::dec
            <<  (tfInfo.m_offset)   <<  ')'
            <<  "\n\tsize    = 0x"
            <<  std::hex    <<  std::setw(8)
            <<  (tfInfo.m_size)
            <<  " ("    <<  std::dec
            <<  (tfInfo.m_size)     <<  ')'
            <<  "\n\tcrc     = 0x"
            <<  std::hex    <<  std::setw(8)
            <<  (tfInfo.m_crc32);
    return ( outStr );
}

void
parseIndex(
        const   Buffer      ptrBegin,
        const   Buffer      ptrEnd,
        const   uint64_t    offset,
        std::ostream      & outStr)
{
    int64_t             posCur  = offset;
    std::string         lastKey;
    std::stringstream   ss;

    for ( Buffer ptr = ptrBegin + offset; ptr < ptrEnd; ) {
        ss.clear();
        ss.str("");

        ss  <<  std::setfill('0')
            <<  std::setw(8)    <<  std::hex
            <<  posCur          <<  '\t';

        int64_t    posNew  = posCur;
        Buffer      ptrSave = ptr;

        uint64_t    shared  = readVarInt<uint64_t>(ptr, ptrEnd, posNew);
        uint64_t    nshared = readVarInt<uint64_t>(ptr, ptrEnd, posNew);
        uint64_t    valSize = readVarInt<uint64_t>(ptr, ptrEnd, posNew);

#if defined( _DEBUG )
        std::cerr   <<  "# DEBUG shared = "
                    <<  shared
                    <<  ", nshared = "  <<  nshared
                    <<  ", valsize = "  <<  valSize
                    <<  ", lastKey = "  <<  lastKey
                    <<  std::endl;
#endif

        if ( (shared == 0) && (nshared == 0) && (valSize == 0) ) {
            break;
        }

        std::string key;
        if ( shared > 0 ) {
            key.assign(lastKey, 0, static_cast<size_t>(shared));
        }
        key     += std::string(ptr, ptr + nshared);
        ptr     += nshared;
        posNew  += nshared;

        ss  <<  key;

        BundleEntry     tfInfo;
        parseProtocolBuffer(ptr, ptr + valSize, posNew, &tfInfo, 0);

        ss  <<  '\n'
            <<  tfInfo  <<  std::endl;

        ptr     += valSize;
        posNew  += valSize;
        outStr  <<  ss.str();

        lastKey =  key;
        posCur  =  posNew;
    }
}

int  main(int argc, char * argv[])
{
    FILE *  fp  = fopen(argv[1], "rb");
    fseek(fp, 0, SEEK_END);
    size_t  cbSize  = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    std::vector<uint8_t>    buffer(cbSize);
    fread(&buffer[0], 1, cbSize, fp);
    fclose(fp);

    uint64_t    offset  = 0x0009;
    if ( argc > 2 ) {
        size_t  idx = 0;
        offset  = stol(std::string(argv[2]), &idx);
    }

    std::string     input, output;
    input.assign(pointer_cast<const char *>(&buffer[0]), cbSize);

    if ( !snappy::Uncompress(input.data(), input.size(), &output) )
    {
        std::cerr   <<  "Uncompressed Snappy OK: "
                    <<  "Size = "   <<  output.size()
                    <<  std::endl;
        const   Buffer  ptr = pointer_cast<Buffer>(output.data());
        fp  = fopen("uncompressed.bin", "wb");
        fwrite(ptr, 1, output.size(), fp);
        fclose(fp);

        parseIndex(ptr, ptr + output.size(), offset, std::cout);
    } else{
        std::cerr   <<  "Uncompressed Snappy FAILED. "
                    <<  "Treat as Not-Compressed Format."
                    <<  std::endl;
        const   Buffer  ptr = &(buffer[0]);
        parseIndex(ptr, ptr + cbSize, offset, std::cout);
    }

    return ( 0 );
}
